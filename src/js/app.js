import { masterMap } from './utils/_masterMap';

if (document.querySelector('.map__elementMap')) {
  (async function mapBranch() {
    try {
      let map = new masterMap();
      let getbranchs = await map.getaBrach();
      map.createMap('.map__elementMap', getbranchs, 6, { 
        lat: 20.6122835, 
        lng: -100.4802578 
      });
    } catch (error) {
      console.warn(`Exception in mapBranch => ${error}`);
    }
  })();
}