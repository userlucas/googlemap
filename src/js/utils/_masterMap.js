import { Loader } from "@googlemaps/js-api-loader";

let InforObj = [];

class masterMap {

  init(element = "") {

    if (!document.querySelector(element)) return {
      object: false,
      message: 'Exception in init() => Element not found in DOM'
    };

    return {
      object: new Loader({
        apiKey: "AIzaSyAUZd0B-9YNIiIAF6TkYb20Q61AYveHHyo",
        version: "weekly",
      }),
      message: 'success'
    }

  }

  async createMap(element = '', markers = [], zoom = 6, center = { lat: 0, lng: 0 }) {
    let initMap = await this.init(element);

    if (!initMap.object) return console.warn(initMap.message);
    let mapElement = document.querySelector(element);

    const renderMap = entries => {

      entries.forEach(entry => {
        if (entry.isIntersecting) {
          initMap.object.load()
            .then(event => {
              let geocoder = new google.maps.Geocoder();
              let styledMapType = new google.maps.StyledMapType(mapSkin);

              let map = new google.maps.Map(mapElement, {
                center: center,
                zoom: zoom,
                mapTypeControl: false,
                scrollwheel: false,
                mapTypeId: "stesso",
              });

              markers.forEach(element => this.geocodeAddress(geocoder, element, map));

              map.mapTypes.set("styled_map", styledMapType);
              map.setMapTypeId("styled_map");
              googleMapsObserver.unobserve(mapElement);

            });
        }
      });

    };

    const googleMapsObserver = new IntersectionObserver(renderMap, {
      root: null,
      threshold: 0.1
    });
    googleMapsObserver.observe(mapElement);

  }

  geocodeAddress(geocoder, markets, resultsMap) {
    //icon of marker 
    let icon = {
      url: "./web/img/marker-cdmx.svg",
      scaledSize: new google.maps.Size(37, 50),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(39, 10)
    };

    geocoder.geocode({ address: markets.address}, (results, status) => {
      if (status === "OK") {
        resultsMap.setCenter(results[0].geometry.location);
        let marker = new google.maps.Marker({
          map: resultsMap,
          position: results[0].geometry.location,
          icon: icon,
          markerInfo: markets
        });
        this.attachSecretMessage(marker);
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }

  attachSecretMessage(marker) {
    const infowindow = new google.maps.InfoWindow();
    let markerInfo = marker.markerInfo;
    //info window of marker 
    var contentString = `
      <article class="container markerInfo">
        <div class="col-12 py-2">
          <h3 class="title">${markerInfo.title}</h3> 
          <p class="description mb-0 py-1">${markerInfo.description}</p>
          <span class="address mb-0 py-2">${markerInfo.address}</span>
          <p class="phone d-block"><a href="tel:${markerInfo.phone}">${markerInfo.phone}</a> </p>
        </div>
      </article>
    `;

    infowindow.setContent(contentString);

    marker.addListener("click", event => {
      this.closeOtherInfo();
      infowindow.open(marker.get("map"), marker);
      InforObj[0] = infowindow;
    });
  }

  closeOtherInfo() {
    if (InforObj.length > 0) {
      InforObj[0].set("marker", null);
      InforObj[0].close();
      InforObj.length = 0;
    }
  }

  async getaBrach(branch = '', GetAll = true) {

    if (GetAll) return branchsGlobals;

    return branchArray = branchsGlobals.filter(item => {
      if (item.title.includes(branch)) return item;
    });

  }
}

export { masterMap };